#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QLabel>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_server_clicked()
{
    socket->connectToHost("127.0.0.1",6666);
    socket->write("");
    socket->waitForReadyRead(500);
     QString status;
     status = socket->readAll();
     ui->status->setText(status);
}

void MainWindow::sockDisc()
{
    socket->deleteLater();
}

void MainWindow::on_b1_clicked()
{
    ui->pole->clear();
    socket->write("1\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}

void MainWindow::on_b2_clicked()
{
    socket->write("2\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}


void MainWindow::on_b3_clicked()
{
    socket->write("3\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}

void MainWindow::on_b4_clicked()
{
    socket->write("4\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}

void MainWindow::on_b5_clicked()
{
    socket->write("5\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}

void MainWindow::on_b6_clicked()
{
    socket->write("6\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}

void MainWindow::on_b7_clicked()
{
    socket->write("7\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}

void MainWindow::on_b8_clicked()
{
    socket->write("8\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}

void MainWindow::on_b9_clicked()
{
    socket->write("9\r\n");
    socket->waitForReadyRead(500);
     QString qstr;
     qstr = socket->readAll();
     ui->pole->setText(qstr);
}

void MainWindow::on_update_clicked()
{
    socket->waitForReadyRead(500);

    QString qstr;
    qstr = socket->readAll();
    ui->pole->setText(qstr);
}
