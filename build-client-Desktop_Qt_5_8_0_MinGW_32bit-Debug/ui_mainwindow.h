/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *pole;
    QLabel *status;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QPushButton *b4;
    QPushButton *b2;
    QPushButton *b1;
    QPushButton *b8;
    QPushButton *b9;
    QPushButton *b7;
    QPushButton *b6;
    QPushButton *b3;
    QPushButton *b5;
    QPushButton *update;
    QPushButton *server;
    QMenuBar *menuBar;
    QMenu *menuclient2;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(703, 296);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 0, 701, 221));
        horizontalLayout = new QHBoxLayout(layoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        pole = new QLabel(layoutWidget);
        pole->setObjectName(QStringLiteral("pole"));

        verticalLayout_2->addWidget(pole);

        status = new QLabel(layoutWidget);
        status->setObjectName(QStringLiteral("status"));

        verticalLayout_2->addWidget(status);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        b4 = new QPushButton(layoutWidget);
        b4->setObjectName(QStringLiteral("b4"));

        gridLayout->addWidget(b4, 1, 0, 1, 1);

        b2 = new QPushButton(layoutWidget);
        b2->setObjectName(QStringLiteral("b2"));

        gridLayout->addWidget(b2, 0, 1, 1, 1);

        b1 = new QPushButton(layoutWidget);
        b1->setObjectName(QStringLiteral("b1"));

        gridLayout->addWidget(b1, 0, 0, 1, 1);

        b8 = new QPushButton(layoutWidget);
        b8->setObjectName(QStringLiteral("b8"));

        gridLayout->addWidget(b8, 2, 1, 1, 1);

        b9 = new QPushButton(layoutWidget);
        b9->setObjectName(QStringLiteral("b9"));

        gridLayout->addWidget(b9, 2, 2, 1, 1);

        b7 = new QPushButton(layoutWidget);
        b7->setObjectName(QStringLiteral("b7"));

        gridLayout->addWidget(b7, 2, 0, 1, 1);

        b6 = new QPushButton(layoutWidget);
        b6->setObjectName(QStringLiteral("b6"));

        gridLayout->addWidget(b6, 1, 2, 1, 1);

        b3 = new QPushButton(layoutWidget);
        b3->setObjectName(QStringLiteral("b3"));

        gridLayout->addWidget(b3, 0, 2, 1, 1);

        b5 = new QPushButton(layoutWidget);
        b5->setObjectName(QStringLiteral("b5"));

        gridLayout->addWidget(b5, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout);

        update = new QPushButton(layoutWidget);
        update->setObjectName(QStringLiteral("update"));

        verticalLayout->addWidget(update);

        server = new QPushButton(layoutWidget);
        server->setObjectName(QStringLiteral("server"));

        verticalLayout->addWidget(server);


        horizontalLayout->addLayout(verticalLayout);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 703, 25));
        menuclient2 = new QMenu(menuBar);
        menuclient2->setObjectName(QStringLiteral("menuclient2"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuclient2->menuAction());

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        pole->setText(QString());
        status->setText(QString());
        b4->setText(QApplication::translate("MainWindow", "4", Q_NULLPTR));
        b2->setText(QApplication::translate("MainWindow", "2", Q_NULLPTR));
        b1->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        b8->setText(QApplication::translate("MainWindow", "8", Q_NULLPTR));
        b9->setText(QApplication::translate("MainWindow", "9", Q_NULLPTR));
        b7->setText(QApplication::translate("MainWindow", "7", Q_NULLPTR));
        b6->setText(QApplication::translate("MainWindow", "6", Q_NULLPTR));
        b3->setText(QApplication::translate("MainWindow", "3", Q_NULLPTR));
        b5->setText(QApplication::translate("MainWindow", "5", Q_NULLPTR));
        update->setText(QApplication::translate("MainWindow", "\320\236\320\261\320\275\320\276\320\262\320\270\321\202\321\214", Q_NULLPTR));
        server->setText(QApplication::translate("MainWindow", "\320\241\320\265\321\200\320\262\320\265\321\200", Q_NULLPTR));
        menuclient2->setTitle(QApplication::translate("MainWindow", "client2", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
